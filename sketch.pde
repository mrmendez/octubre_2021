Item tmp;
Item barras[];
float xpos, ypos;
int anchoBarra;
int altoBarra;
String mensaje;
PFont pf;
void setup(){
    pf = loadFont("Arial-BoldMT-32.vlw");
    textFont(pf);
    size(700,400);
    xpos = width-300;
    ypos = 30;
    anchoBarra = 20;
    altoBarra = 60;
    barras = new Item[12];
    for(int i = 0;i < barras.length;i++){
       barras[i] = new Item(xpos+(i*20)+(5*i), ypos,anchoBarra, altoBarra, i);
    }
    mensaje = "algo";
}

void draw(){
    background(170);
    for(int i=0; i < barras.length;i++){
        barras[i].dibujarItem();
    }
    fill(0);
    text(mensaje,40,170);
}
void mouseClicked(){
     mensaje = "NO SELECCIONAS NINGUN COLOR";
     for(int i=0;i < barras.length;i++){
        if( barras[i].isColision(mouseX, mouseY) ){
            mensaje = barras[i].id;
            break;
        }
     }
}