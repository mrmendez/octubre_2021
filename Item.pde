class Item{
   float xp;
   float yp;
   float ancho;
   float alto;
   color rojo;  // COLORES
   color naranja;
   color amarillo;
   color cafe;
   color negro;
   color verde;
   color azul;
   color blanco;
   color gris;
   color violeta;
   color dorado;
   color plata;
   color disponibles[];
   int indiceColor;
   String id;
   Item(float a, float b, float c, float d, int e){
       xp =a;
       yp = b;
       ancho = c;
       alto = d;
       rectMode(CORNER);
       String nombre[] = {"negro","cafe","rojo","naranja","amarillo", "verde", "azul",
                          "violeta", "gris", "blanco", "dorado", "plata"};
       disponibles = new color[12];
       negro = color(0,0,0);        disponibles[0] = negro;     //COLOR 0
       cafe = color(183,95,22);     disponibles[1] = cafe;      //COLOR 1
       rojo = color(250,18,33);     disponibles[2] = rojo;      //COLOR 2
       naranja = color(250,122,18); disponibles[3] = naranja;   //COLOR 3
       amarillo = color(239,250,96);disponibles[4] = amarillo;  //COLOR 4
       verde = color(28,250,18);    disponibles[5] = verde;     //COLOR 5
       azul = color(18,54,250);     disponibles[6] = azul;      //COLOR 6
       violeta = color(250,18,239); disponibles[7] = violeta;   //COLOR 7
       gris = color(172,178,162);   disponibles[8] = gris;      //COLOR 8
       blanco = color(255,255,255); disponibles[9] = blanco;    //COLOR 9
       dorado = color(149,203,49);  disponibles[10] = dorado;   //COLOR 10
       plata = color(220,224,213);  disponibles[11] = plata;    //COLOR 11
       indiceColor = e;
       id = nombre[e];
 }
   
   void dibujarItem(){
       fill(disponibles[indiceColor]);
       rect(xp,yp,ancho,alto);
   }
   
   boolean isColision(float a, float b){
       if ( a>= xp && a <= (xp+ancho)  ){
           if(b >= yp && b <= yp+alto){
              return true;
           }else{return false;}
       }else{return false;}
   }
   
   
}